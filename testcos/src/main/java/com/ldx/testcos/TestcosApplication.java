package com.ldx.testcos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestcosApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestcosApplication.class, args);
    }

}
